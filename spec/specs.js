describe("A suite", function() {
  it("contains spec with an expectation", function() {
    expect(true).toBe(true);
  });
});

/////////////////////////////////////
//  Cell class, central unit of grid
//
describe("A Cell", function() {
  it("has a row", function() {
     expect(new Cell(1,0).row).toBe(1);
  });
});

describe("A Cell", function() {
  var c = undefined;
  beforeEach(function() {
    c = new Cell(5,9);
  });
  
  it("has a row", function() {
     expect(c.row).toBe(5);
  });
});
