function ToCanvas(canvas, context, _funcrgb = null, _col_walls = null, _w_walls = null) {

    var canvas = canvas;
    var context = context;
    var range = 1;
    var color_scale = (_funcrgb == null) ? funcrgb : _funcrgb;
    var color_walls = (_col_walls == null) ? "yellow" : _col_walls;
    var width_walls = (_w_walls == null) ? 1 : _w_walls;


    return function show(grid, _root = null, cell_size = 10) {
        var root = (_root == null) ? "0,0" : _root;
        var dists = grid.distances(root);
        var arr_max = grid.max(root);
        range = arr_max[1];
        grid.each_cell(function(cell) {
            var x1 = cell.col * cell_size;
            var y1 = cell.row * cell_size;
            var x2 = (cell.col + 1) * cell_size;
            var y2 = (cell.row + 1) * cell_size;
            
            cell_box(x1, y1, x2, y2, dists.get_dist(cell.name));

            if (cell.north == null)           { line(x1, y1, x2, y1); }
            if (cell.west == null)            { line(x1, y1, x1, y2); }
            if (!cell.is_linked(cell.east))   { line(x2, y1, x2, y2); }
            if (!cell.is_linked(cell.south))  { line(x1, y2, x2, y2); }

        })
    }

    function funcrgb(dist, range) {
        return "rgb("+parseInt(255 - dist*255/range) + ",0," + parseInt(dist*255/range)+ ")";
    }

    function cell_box(x1, y1, x2, y2, dist = 0) {
        context.beginPath();
        context.rect(x1, y1, x2 - x1, y2 - y1);
        context.fillStyle = color_scale(dist, range);
        context.fill();
    }

    function line(x1, y1, x2, y2) {
        context.beginPath();
        context.lineWidth = width_walls;
        context.moveTo(x1,y1);
        context.strokeStyle = color_walls;
        context.lineTo(x2,y2);
        context.stroke();
    }
}
