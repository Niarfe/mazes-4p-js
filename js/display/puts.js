function puts(grid, dists = null) {
    var output = ""
    output += "+" + "---+".repeat(grid.num_cols) + "\n";

    grid.each_row(function(row) {
        var top = "|";
        var bottom = "+";

        row.forEach(function(cell) {
           var body = (dists != null && dists.get_dist(cell.name) != null) ? " " + dists.get_dist(cell.name) + " " : "   ";
           if (body.length > 3) { body = body.slice(0,-1); }
           var east_boundary = (cell.is_linked(cell.east)) ? " " : "|";

           top += body + east_boundary;
           var south_boundary = (cell.is_linked(cell.south)) ? "   " : "---";
           var corner = "+";
           bottom += south_boundary + corner;
        });
        output += top + "\n";
        output += bottom + "\n";
    })
    console.log("\n"+output);
    return "\n"+output;
}