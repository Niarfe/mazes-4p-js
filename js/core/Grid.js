
class Grid {
    constructor(num_rows, num_cols) {
        this.num_rows = num_rows;
        this.num_cols = num_cols;
        this.inner_grid = [];
        this.inner_grid = this.prepare_grid();

        this.configure_cells();
    }

    prepare_grid() {
        for (var row = 0; row < this.num_rows; row++) {
            var row_array = [];
            for (var col = 0; col < this.num_cols; col++) {
                row_array.push(new Cell(row, col));
            }
            this.inner_grid.push(row_array);
        }
        console.log("Inner grid size is " + this.inner_grid.length.toString());
        return this.inner_grid
    }

    configure_cells() {
        for (var r = 0; r < this.num_rows; r++) {
            for (var c = 0; c < this.num_cols; c++) {
                var cell = this.inner_grid[r][c];
                var row = cell.row;
                var col = cell.col;
                cell.north = this.get_cell(row - 1, col);
                cell.south = this.get_cell(row + 1, col);
                cell.west  = this.get_cell(row, col - 1);
                cell.east  = this.get_cell(row, col + 1);
             }
        }
    }

    get_grid() {
        return this.inner_grid;
    }

    get_cell(r, c) {
        if (r < 0 || c < 0) { return null; }
        if (r >= this.num_rows || c >= this.num_cols) { return null; }
        return this.inner_grid[r][c];
    }

    get_cell_by_name(name) {
        var r = parseInt(name.split(",")[0]);
        var c = parseInt(name.split(",")[1]);
        return this.get_cell(r,c);
    }

    each_row(fun) {
        this.inner_grid.forEach(function(row) {
            fun(row);
        });
    }

    each_cell(fun, _this) {
        this.inner_grid.forEach(function(row) {
            row.forEach(function(cell) {
                fun(cell);
            }, _this );
        }, _this);
    }

    random_cell() {
        var random_row = this.inner_grid[Math.floor(Math.random()*this.inner_grid.length)];
        var random_cell = random_row[Math.floor(Math.random()*random_row.length)];
        return random_cell;
    }

    size() {
        return (this.num_rows * this.num_cols);
    }

    distances(root_name) {
        var dists = new Distances(root_name);
        var frontier = [root_name];

        while (frontier.length > 0) {
            var new_frontier = [];

            for (let cell_name of frontier) {
                this.get_cell_by_name(cell_name).get_links().forEach(function(linked) {
                    if (dists.cells[linked] == null) {
                        dists.set_cell(linked,  dists.get_dist(cell_name) + 1);
                        new_frontier.push(linked);
                    }
                });
            };
            frontier = new_frontier;
        }
        return dists;
    }

    path_to_goal(root, goal) {
        var current = goal;
        var dists = this.distances(root);
        var breadcrumbs = new Distances(root);
        breadcrumbs.set_cell(current, dists.get_dist(current));
        self = dists;
        do {
            this.get_cell_by_name(current).get_links().forEach(function(neighbor) {
                if (self.cells[neighbor] < self.cells[current]) {
                    breadcrumbs.set_cell(neighbor, self.get_dist(neighbor));
                    current = neighbor;
                }
            }, self);

        } while (current != root);
        return breadcrumbs;
    }

    max(root) {
        var max_distance = 0;
        var max_cell = root;
        var dists = this.distances(root);
        self = this;

        Object.keys(dists.cells).forEach(function(cname) {
            if (dists.cells[cname] > max_distance) {
                max_cell = cname;
                max_distance = dists.cells[cname];
            }
        });
        return [max_cell, max_distance];
    }

}



