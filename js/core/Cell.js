
class Cell {
    constructor(row, col) {
        this.row = row;
        this.col = col;
        this.name = row.toString() + "," + col.toString();

        this.north = null;
        this.south = null;
        this.west = null;
        this.east = null;

        this.links = {};
        this.dist = null;
    }

    link(cell, bidi = true) {
        this.links[cell.toString()] = true;
        if (bidi) {
            cell.link(this, false);
        }
        return this;
    }

    unlink(cell, bidi = true) {
        this.links.delete(cell.toString());
        if (bidi) {
            cell.unlink(self, false);
        }
        return this;
    }

    get_links() {
       return Object.keys(this.links);
    }

    is_linked(cell) {
        if (cell == null) { return false; }
        return (cell.toString() in this.links);
    }

    neighbors() {
        var list = [];
        if (this.north) { list.push(this.north); }
        if (this.south) { list.push(this.south); }
        if (this.east) { list.push(this.east); }
        if (this.west) { list.push(this.west); }
        return list;
    }

    toString() {
        return this.name;
    }

    get_content() {
        return "`u'";
    }
}