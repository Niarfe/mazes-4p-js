class Distances {

    constructor(root_name) {
        this.root_name = root_name;
        this.cells = {};
        this.cells[this.root_name] = 0;
    }

    get_dist(cell_name) {
        return this.cells[cell_name];
    }

    set_cell(cell_name, distance) {
        this.cells[cell_name] = distance;
    }

    get_cells() {
        return Object.keys(this.cells);
    }


}