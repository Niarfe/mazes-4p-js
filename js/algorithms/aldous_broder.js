(function(_module) {
    //var module = {};

    _module.AldousBroder = function(grid) {
        console.log("AldousBroder");

        var cell = grid.random_cell();
        var unvisited = (grid.num_rows * grid.num_cols) - 1;
        while (unvisited > 0) {
            var neighbor = cell.neighbors()[Math.floor(Math.random()*cell.neighbors().length)];
            if (Object.keys(neighbor.links).length == 0) {
                cell.link(neighbor);
                unvisited -= 1;
            }
            cell = neighbor;
        }
        return grid;
    }

    //return module;
})(algorithms);
