(function(_module) {
    _module.SideWinder = function(grid) {
        console.log("SideWinder");

        grid.each_row(function(row) {
            var run = [];

            row.forEach(function(cell) {
                run.push(cell);
                var at_eastern_boundary = (cell.east == null);
                var at_northern_boundary = (cell.north == null);

                var should_close_out = at_eastern_boundary || (!at_northern_boundary && rand(2) == 0);

                if (should_close_out) {
                    var member = sample(run);
                    if (member.north != null) {
                        member.link(member.north);
                    }
                    run = [];
                } else {
                    cell.link(cell.east);
                }
            });
        });
        return grid;
    }

    function rand(num) {
           return Math.floor(Math.random()*num);
    }

    function sample(arr) {
        return arr[Math.floor(Math.random()*arr.length)];
    }
})(algorithms);
