var algorithms = (function() {
    var module = {};

    module.BinaryTree = function(grid) {
        console.log("Binary Tree");

        grid.each_cell(function(cell) {
            var neighbors = [];
            if (cell.north != null && cell.north.constructor == Cell) { neighbors.push(cell.north);}
            if (cell.east != null && cell.east.constructor == Cell) { neighbors.push(cell.east);}

            var index = Math.floor(Math.random()*neighbors.length);
            var neighbor = neighbors[index];
            if (neighbor) {
                cell.link(neighbor);
            }
        })
        return grid;
    }


    return module;
})();
